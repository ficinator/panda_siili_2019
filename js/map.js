(function($) {

	var map,
	    postsLayer,
		postList = document.getElementById('post-list'),
		asidePostList = $('aside.post-list'),
		postListToggle = $('#post-list-toggle');

	function loadMap() {
		initMap();
		wp.api.loadPromise.done(function() {
			// new wp.api.collections.Media().fetch({success: (mediaCollection) => {
			// 	let media = {};
			// }})
			new wp.api.collections.Categories().fetch({success: (categoriesCollection) => {
				new wp.api.collections.Posts().fetch({
					data: {
						// filter: {per_page: 1000}
						per_page: 100
					},
					success: (postsCollection) => {
						let postsGeojson = getPostsGeoJson(categoriesCollection, postsCollection);
						// add post items into the list
						buildPostList(postsGeojson);
						// display layer with post markers on the map
						addPostLayer(postsGeojson);
					}
				});
			}});
		});
	}

	function initMap() {
		setPostListToggleClass();
		let mapContainer = document.querySelector('.post-list-map-container');
		adjustMapHeight(mapContainer);
		map = L.map('world-map', {
			center: [50, 35],
			zoom: 3,
			zoomControl: false,
		});
		let zoomControl = new L.Control.Zoom({position: 'bottomleft'}).addTo(map);
		let mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; ' + mapLink + ' contributors'
		}).addTo(map);
		// needs an API key (150k views for free) https://www.thunderforest.com/maps/
		// let ocmlink = '<a href="http://thunderforest.com/">Thunderforest</a>';
		// L.tileLayer('http://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png', {
		// 	attribution: '&copy; ' + mapLink + ' Contributors & '+ocmlink,
		// 	// maxZoom: 18,
  //   	}).addTo(map);
	}

	function buildPostList(postsGeojson) {
		// Iterate through the list of posts
		for (let i in postsGeojson.features) {
			let postFeature = postsGeojson.features[i];
			// Select the post-list container in the HTML and append a li
			// with the class 'post-item' for each store
			let postItem = postList.appendChild(document.createElement('li'));
			postItem.className = 'post-item';
			postItem.id = 'post-item-' + postFeature.properties.slug;
			postItem.dataPosition = i;

			// Create a new heading with the class 'title' for each post
			let title = postItem.appendChild(document.createElement('h3'));
			title.className = 'post-title';
			title.innerHTML = postFeature.properties.title;

			// Create post details div
			let details = postItem.appendChild(document.createElement('div'));
			details.className = 'post-details';

			let icon = details.appendChild(document.createElement('img'));
			icon.src = getCategoryImagePath(postFeature.properties.categories);

			// create link to the post itself
			let link = details.appendChild(document.createElement('a'));
			link.href = postFeature.properties.link;
			link.className = 'post-read-more';
			// TODO: solve localization
			link.innerHTML = 'Read More';

			// add listener to fly to map location and highlight to item in the post list
			postItem.addEventListener('click', function(e) {
				let feature = postsGeojson.features[this.dataPosition];
				flyToFeature(feature);
				highlightPostItem(this);
				showFeaturePopUp(feature);
			});
		}
	}

	function addPostLayer(postsGeojson) {
		// let markers = L.markerClusterGroup();
		postsLayer = L.geoJSON(postsGeojson, {
			pointToLayer: function (feature, latlng) {
				return L.marker(latlng, {
					icon: L.icon({
						iconUrl: getCategoryImagePath(feature.properties.categories, isPin = true),
						iconSize: [64, 80], // size of the icon
						shadowSize:   [64, 80], // size of the shadow
						iconAnchor: [32, 80], // point of the icon which will correspond to marker's location
						shadowAnchor: [32, 80],  // the same for the shadow
						popupAnchor: [6, -80] // point from which the popup should open relative to the iconAnchor
					}),
				});
			},
			onEachFeature: function (feature, layer) {
				let properties = feature.properties;
				let title = properties.title;
				let link = properties.link;
				let slug = properties.slug;
				let category_links = [];
				for (let category of properties.categories) {
					category_links.push('<a href="' + category.link + '">' + category.name + '</a>')
				}
				layer.bindPopup(
					'<h3 class="entry-title"><a href="' + link + '">' + title + '</a></h3>' +
					'<span class="cat-links">' + category_links.join(', ') + '</div>'
				);
				layer.on({
					click: function(e) {
						let feature = e.target.feature
						flyToFeature(feature);
						let postItem = document.getElementById('post-item-' + feature.properties.slug);
						highlightPostItem(postItem);
					}
					// TODO: add something like on popup closed
				});
				layer.slug = slug;
			}
		});
		// markers.addLayer(postsLayer);
		// map.addLayer(markers);
		map.addLayer(postsLayer);
	}

	function getCategoryImagePath(categories, isPin = false) {
		let imagePath = uri.theme_file + '/images/' + (isPin ? 'map' : 'categories') + '/'; 
		imagePath += categories.length === 0 || categories[0].slug === 'uncategorized' ? 'banana' : categories[0].slug;
		if (isPin) {
			imagePath += '_pin'
		}
		return imagePath + '.png'
	}

	function getPostsGeoJson(categoriesCollection, postsCollection) {
		let features = [];
		for (let post of postsCollection.models) {
			let attribs = post.attributes;
			let meta = attribs.meta;
			if (meta.latitude && meta.longitude) {
				let postCategories = [];
				for (category_id of attribs.categories) {
					postCategories.push(categoriesCollection._byId[category_id].attributes)
				}
				features.push({
					'type': 'Feature',
					'properties': {
						'title': attribs.title.rendered,
						'link': attribs.link,
						'slug': attribs.slug,
						'categories': postCategories,
					},
					'geometry': {
						'type': 'Point',
						'coordinates': [meta.longitude[0], meta.latitude[0]]
					}
				});
			}
		}
		return {'type': 'FeatureCollection', 'features': features}
	}

	function flyToFeature(feature) {
		map.flyTo(feature.geometry.coordinates.slice().reverse(), 10);
	}

	function highlightPostItem(postItem) {
		let highlightedItems = postList.querySelector('.highlighted');
		if (highlightedItems) {
			highlightedItems.classList.remove('highlighted');
		}
		postItem.classList.add('highlighted');
	}

	function showFeaturePopUp(feature) {
		let slug = feature.properties.slug;
		for (let layer of postsLayer.getLayers()) {
			if (layer.slug == slug) {
				layer.openPopup();
			}
		}
	}

	// toggle class of the post list toggle button according to visibility of the aside post list
	function setPostListToggleClass() {
		postListToggle.toggleClass('visible', asidePostList.is(':visible'));
	}

	// toggle aside post list and set the post list toggle button class
	function toggleAsidePostList() {
		// $('aside.post-list').animate({width: 'toggle'}, 200);
		if (asidePostList.is(':visible')) {
			asidePostList.removeClass('visible');
			asidePostList.addClass('hidden');
		}
		else {
			asidePostList.removeClass('hidden');
			asidePostList.addClass('visible');
		}
		setPostListToggleClass();
	}

	// on document load, load the map
	document.addEventListener('DOMContentLoaded', loadMap);

	// on window resize, adjust the map height
	window.addEventListener('resize', function(event) {
		let mapContainer = document.querySelector('.post-list-map-container');
		adjustMapHeight(mapContainer);
		setPostListToggleClass();
	});

	postListToggle.click(toggleAsidePostList);
	$('#post-list-close').click(toggleAsidePostList);

})(jQuery);
