(function($) {
	var map,
		pandaSiiliMarker,
		countdownElem = $('#countdown'),
		daysElem = $('#days'),
		hoursElem = $('#hours'),
		minutesElem = $('#minutes'),
		secondsElem = $('#seconds'),
		startDate = new Date('Jan 3, 2020 13:00:00').getTime(),
		// startDate = new Date('Jan 11, 2020 23:20:00').getTime(),
		endDate = new Date('Apr 30, 2020 16:00:00').getTime();
		// endDate = new Date('Jan 11, 2020 23:21:00').getTime();

	function initMap() {
		let mapContainer = document.querySelector('.map-container');
		adjustMapHeight(mapContainer, 100);

		map = L.map('world-map', {
			scrollWheelZoom: false,
			dragging: !L.Browser.mobile,
		}).setView([50, 16], 6);
		let mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; ' + mapLink + ' contributors'
		}).addTo(map);
	}

	function computeCurrentPointIndex(startDate, endDate, length) {
		let now = new Date().getTime();
		if (now <= startDate) {
			return 0;
		}
		if (now >= endDate) {
			return length - 1;
		}
		return Math.floor((now - startDate) / (endDate - startDate) * length);
	}


	function initRoute(routeGeojson) {
		// add route line string
		let routeLayers = L.geoJSON(routeGeojson, {
			style: {
				color: 'black',
				weight: 5,
			}
		}).addTo(map);

		// get line string points
		let routePoints = routeGeojson.features[0].geometry.coordinates[0]
		let numRoutePoints = routePoints.length

		// add static hut marker to the last point
		let hutIcon = L.icon({
			iconUrl: uri.theme_file + '/images/u_horniku.png',
			// shadowUrl: 'leaf-shadow.png',
			iconSize: [220, 150], // size of the icon
			// shadowSize:   [50, 64], // size of the shadow
			iconAnchor: [110, 110], // point of the icon which will correspond to marker's location
			// shadowAnchor: [4, 62],  // the same for the shadow
			popupAnchor: [-23, -117], // point from which the popup should open relative to the iconAnchor
		});
		let endCoordinates = routePoints[numRoutePoints - 1]
		let hutMarker = L.marker(
			[endCoordinates[1], endCoordinates[0]],
			{
				icon: hutIcon,
			},
		).addTo(map).bindPopup(
			'<b>Chalupa u Horníku</b><br>Vítkovice v Krkonoších 334<br>512 38 Česká Republika'
			);

		// add panda & siili marker
		let pandaSiiliIcon = L.icon({
			iconUrl: uri.theme_file + '/images/panda_siili.png',
			iconSize: [100, 100],
			iconAnchor: [50, 50],
			popupAnchor: [5, -60],
		});
		let routePointIndex = computeCurrentPointIndex(startDate, endDate, numRoutePoints);
		let coordinates = routePoints[routePointIndex]
		pandaSiiliMarker = L.marker(
			[coordinates[1], coordinates[0]],
			{
				icon: pandaSiiliIcon,
			}
		).addTo(map).bindPopup('<b>Uff...</b>');
	}

	function updateMarker(routeGeojson) {
		// get line string points
		let routePoints = routeGeojson.features[0].geometry.coordinates[0]
		let numRoutePoints = routePoints.length
		let routePointIndex = computeCurrentPointIndex(startDate, endDate, numRoutePoints);
		let coordinates = routePoints[routePointIndex];
		pandaSiiliMarker.setLatLng([coordinates[1], coordinates[0]]);
	}

	function initCountdown(routeGeojson) {
		// Update the count down every 1 second
		let interval = setInterval(function() {

		  // Get today's date and time
		  let now = new Date().getTime();

		  // Find the distance between now and the count down date
		  let distance = endDate - now;

		  // Time calculations for days, hours, minutes and seconds
		  daysElem.text(Math.floor(distance / (1000 * 60 * 60 * 24)));
		  hoursElem.text(Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)));
		  minutesElem.text(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
		  secondsElem.text(Math.floor((distance % (1000 * 60)) / 1000));

		  // update panda & siili's location
		  updateMarker(routeGeojson);

		  // If the count down is finished, write some text
		  if (distance < 0) {
		    clearInterval(interval);
		    countdownElem.text(translation.expired);
		  }
		}, 1000);
	}

	document.addEventListener('DOMContentLoaded', function() {
		initMap();
		let routeUrl = uri.theme_file + '/assets/tracks.geojson'
		$.getJSON(routeUrl, function(routeGeojson) {
			initRoute(routeGeojson);
			initCountdown(routeGeojson);
		});
	});

})(jQuery);
