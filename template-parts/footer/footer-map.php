<footer id="colophon" class="site-footer">
    <div class="site-info">

        <?php if ( has_nav_menu( 'social' ) ) : ?>
            <nav class="social-navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentynineteen' ); ?>">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'social',
                        'menu_class'     => 'social-links-menu',
                        'link_before'    => '<span class="screen-reader-text">',
                        'link_after'     => '</span>' . twentynineteen_get_icon_svg( 'link' ),
                        'depth'          => 1,
                    )
                );
                ?>
            </nav><!-- .social-navigation -->
        <?php endif; ?>

        <span class="site-copyright">
            <?php printf(__('&copy %s Mr. Pig Productions', 'panda_siili_2019' ), date("Y")); ?>
        </span>

    </div><!-- .site-info -->
</footer><!-- #colophon -->
