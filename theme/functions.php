<?php


// load the translations for the theme
load_theme_textdomain('panda_siili_2019', get_template_directory() . '/languages');

// enqueue scripts and styles
function panda_siili_scripts() {
	// google font
	wp_enqueue_style('permanent-marker', 'https://fonts.googleapis.com/css?family=Permanent+Marker');
	// global functions
	wp_enqueue_script('panda-siili-functions', get_theme_file_uri('/js/functions.js'), array(), '1.0');
	// leaflet
	wp_enqueue_style('leaflet', 'https://unpkg.com/leaflet@1.5.1/dist/leaflet.css');
	wp_enqueue_script('leaflet', 'https://unpkg.com/leaflet@1.5.1/dist/leaflet.js');
	// // mapbox gl
	// wp_enqueue_script('mapbox-gl', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.js');
	// wp_enqueue_style('mapbox-gl', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.css');
	// pass theme path to the scripts

	// jquery functions
	wp_enqueue_script('panda-siili-riddle', get_theme_file_uri('/js/riddle.js'), array('jquery'), '1.0', true);

	if (is_page_template('page-templates/map.php')) {
		// wp rest api
		wp_enqueue_script('wp-api');
		// // leaflet marker cluster
		// wp_enqueue_style('leaflet-markercluster', 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css');
		// wp_enqueue_style('leaflet-markercluster-default', 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css');
		// wp_enqueue_script('leaflet-markercluster', 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js');
		// map functions
		wp_enqueue_script('panda-siili-map', get_theme_file_uri('/js/map.js'), array(), '1.0', true);
		// pass theme path to the scripts
		wp_localize_script('panda-siili-map', 'uri', array(
			'theme_file' => get_theme_file_uri(),
		));
	}
	elseif (is_page_template('page-templates/countdown.php')) {
		// include jquery for ajax calls
		wp_enqueue_script('jquery');
		// countdown-map functions
		wp_enqueue_script('panda-siili-countdown', get_theme_file_uri('/js/countdown.js'), array('jquery'), '1.0', true);
		// pass theme path to the scripts
		wp_localize_script('panda-siili-countdown', 'uri', array(
			'theme_file' => get_theme_file_uri()
		));
		wp_localize_script('panda-siili-countdown', 'translation', array(
			'hut' => __('hut', 'panda_siili_2019'),
			'expired' => __('expired', 'panda_siili_2019'),
		));
	}
}
add_action('wp_enqueue_scripts', 'panda_siili_scripts');

// customize password-proteted content form
function password_form() {
    global $post;
    $label = 'pwbox-'. $post->ID;
    $o = '<form action="' . esc_url(site_url('wp-login.php?action=postpass', 'login_post')) . '" method="post">
    	<p>' . __("Enter the password to unlock this page.") . "</p>
    	<p>" . __("Use only lower case and put spaces between the words if needed." ) . '</p>
    	<p>
    		<label for="' . $label . '">' . __("Password: ") . '<input name="post_password" id="' . $label . '" type="password" size="30" /></label> <input type="submit" name="Enter" value="' . esc_attr__("Enter") . '" />
    	</p>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'password_form' );

/**
 * Gets the SVG code for a given icon.
 */
function panda_siili_get_icon_svg($icon, $size = 24) {
	$icon_path = get_theme_file_path('images/icons/' . $icon . '.svg');
	if ( file_exists($icon_path) ) {
		$repl = sprintf( '<svg class="svg-icon" width="%d" height="%d" aria-hidden="true" role="img" focusable="false" ', $size, $size );
		$svg  = preg_replace( '/^<svg /', $repl, file_get_contents( $icon_path ) ); // Add extra attributes to SVG code.
		$svg  = preg_replace( "/([\n\t]+)/", ' ', $svg ); // Remove newlines & tabs.
		$svg  = preg_replace( '/>\s*</', '><', $svg ); // Remove white space between SVG tags.
		return $svg;
	}
	return null;
}

/**
 * Get the category image based on the category slug.
 */
function the_category_image() {
	$category_image_path = '/images/categories/' . get_queried_object()->slug . '.png';
	if (file_exists(get_template_directory() . $category_image_path)) {
		echo '<span class="category-image"><img src="' . get_template_directory_uri() . $category_image_path . '" /></span>';
	}
}

/**
 * Placeholder for google analytics tracking code.
 */
function panda_siili_add_googleanalytics() { ?>
 
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-PC62BHT8FQ"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-PC62BHT8FQ');
	</script>

<?php }
add_action('wp_head', 'panda_siili_add_googleanalytics');
